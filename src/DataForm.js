import React from "react";
import './DataForm.scss';
import { Form, Field } from "react-final-form";


const onSubmit = (values) => {
  alert(JSON.stringify(values, 0, 2));
};

const onlyLetters = new RegExp(/^[a-zA-Z]+$/);

const required = value => (value ? undefined : 'Required');
const mustBeLetters = value => (onlyLetters.test(value) ? undefined : 'Letters only');
const mustBeNumber = value => (isNaN(value) ? 'Must be a number' : undefined);
const minValue = min => value => isNaN(value) || value >= min ? undefined : `At least ${ min } years old`;
const maxChar = min => value => String(value).length <= min ? undefined : `Maximum ${ min } characters`

const composeValidators = (...validators) => value => validators.reduce((error, validator) => error || validator(value), undefined);


const DataForm = () => {
  let formData = { stooge: "larry", sauces: [] };

  return (
    <div className="wrapper">
      <Form
        onSubmit={onSubmit}
        initialValues={{ ...formData }}

        render={({ handleSubmit, form, submitting, pristine, values }) => (
          <form onSubmit={handleSubmit} className="form">

            <Field name="firstName" validate={composeValidators(required, mustBeLetters)}>
              {({ input, meta }) => (
                <div>
                  <label>First Name</label>
                  <input {...input} placeholder="First Name"/>
                  {meta.touched && meta.error && <span>{meta.error}</span>}
                </div>
              )}
            </Field>

            <Field name="lastName" validate={composeValidators(required, mustBeLetters)}>
              {({ input, meta }) => (
                <div>
                  <label>Last Name</label>
                  <input {...input} placeholder="Last Name" />
                  {meta.touched && meta.error && <span>{meta.error}</span>}
                </div>
              )}
            </Field>

            <Field name="age" validate={composeValidators(required, mustBeNumber, minValue(18))}>
              {({ input, meta }) => (
                <div>
                  <label>Age</label>
                  <input {...input} type="text" placeholder="Age" />
                  {meta.error && meta.touched && <span>{meta.error}</span>}
                </div>
              )}
            </Field>

            <div>
              <label>Employed</label>
              <Field name="employed" component="input" type="checkbox" />
            </div>
            <div>
              <label>Favorite Color</label>
              <Field name="favoriteColor" component="select">
                <option />
                <option>Red</option>
                <option>Green</option>
                <option>Blue</option>
              </Field>
            </div>

            <div>
              <label>Sauces</label>
              <div className="labels">
                <label>
                  <Field name="sauces" component="input" type="checkbox" value="ketchup" />
                  Ketchup
                </label>
                <label>
                  <Field name="sauces" component="input" type="checkbox" value="mustard" />
                  Mustard
                </label>
                <label>
                  <Field name="sauces" component="input" type="checkbox" value="mayonnaise" />
                  Mayonnaise
                </label>
                <label>
                  <Field name="sauces" component="input" type="checkbox" value="guacamole" />
                  Guacamole
                </label>
              </div>
            </div>
            <div>
              <label>Best Stooge</label>
              <div className="labels">
                <label>
                  <Field name="stooge" component="input" type="radio" value="larry" />
                  Larry
                </label>
                <label>
                  <Field name="stooge" component="input" type="radio" value="moe" />
                  Moe
                </label>
                <label>
                  <Field name="stooge" component="input" type="radio" value="curly" />
                  Curly
                </label>
              </div>
            </div>

            <Field name="notes" validate={composeValidators(maxChar(100))}>
              {({ input, meta }) => (
                <div>
                  <label>Notes</label>
                  <textarea {...input} type="text" placeholder="Notes" />
                  {meta.error && meta.touched && <span>{meta.error}</span>}
                </div>
              )}
            </Field>

            <div className="buttons">
              <button className="primaryBtn" type="submit" disabled={submitting || pristine}>
                Submit
              </button>
              <button className="resetBtn" type="button" onClick={form.reset} disabled={submitting || pristine}>
                Reset
              </button>
            </div>
            <pre>{JSON.stringify(values, 0, 2)}</pre>
          </form>
        )}
      />
    </div>
  );
};

export default DataForm;
