import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import DataForm from './DataForm';

ReactDOM.render(
  <React.StrictMode>
    <DataForm />
  </React.StrictMode>,
  document.getElementById('root')
);
